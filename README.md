# Sneaker App

This app is a sample which is developed under the MVVM+Clean Architecture. Source will be found in **main** branch.

## Features

This app has below features
### Home screen
* Loads list of data when user open the app
* Add item to cart by clicking + icon, here cart stored in shared preference which is a local database.
* Search item based on the name.

### Details Screen
* When user click on the item on dashboard will navigate to Details screen
* When user clicks on the add to cart button will redirect to cart screen

### Cart Screen
* Cart screen which loads the cart items from the database
* User can remove the item from cart which will decrease the total amount and charges as well.
* Checkout button will display a message and clear data from the cart screen.

## Technical Stack
Kotlin\
Android Studio\
Material Design\
RecyclerView ListAdapter\
Constraint Layout\
ViewBinding\
Flow\
Lifecycle Components\
Navigation Component\
Dagger Hilt\
Coil\
Gson\
Shared Preference

## Package
* Core - Which contains the base and common things of app
* DB - Which contains the local database here we use shared preference
* DI - Dependency Injection files will be present here
* FeatureCart - Cart feature related files are present here.
* FeatureSneaker - both home and details screen files are present here.

## Classes to be notified
* BaseFragment - This will contains the common things which is used around app for each and every fragments. It is abstract class will be used by all the classes.
* APIExtension - The loader on and off will be handled commonly in this class extension function.
* FlowLifeCycle - The flow lifecycle will be handled commonly here to make flow UI safe.
* UseCase classes - Which will contains the business logics since we don't have much complex just initiated the class but functions not written over there.
* Repo and Repo Impl - which contains the function to access with the datasource
* Datasource - which supplies data where the database, retrofit and much more data related stuffs. 

## Notes
* Json Data is hardcoded in the Datasource class
* Dark theme not supported
* Test case not added since i am a beginner.
