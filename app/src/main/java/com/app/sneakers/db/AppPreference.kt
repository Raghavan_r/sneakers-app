package com.app.sneakers.db

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder

const val STORE_CART_DATA = "cart"
const val STORE_DASHBOARD = "dashboard_data"

class AppPreference(context: Context) {

    val preferences: SharedPreferences =
        context.getSharedPreferences("prefs", Context.MODE_PRIVATE)


    /**
     * Saves object into the Preferences.
     *
     * @param `object` Object of model class (of type [T]) to save
     * @param key Key with which Shared preferences to
     **/
    fun <T> storeModelValue(objectData: T, key: String) {
        // Convert object to JSON String.
        val jsonString = GsonBuilder().create().toJson(objectData)
        // Save that String in SharedPreferences
        preferences.edit().putString(key, jsonString).apply()
    }

    /**
     * Used to retrieve object from the Preferences.
     *
     * @param key Shared Preference key with which object was saved.
     **/
    inline fun <reified T> getModelValue(key: String): T? {
        val value = preferences.getString(key, null)
        return GsonBuilder().create().fromJson(value, T::class.java)
    }

}