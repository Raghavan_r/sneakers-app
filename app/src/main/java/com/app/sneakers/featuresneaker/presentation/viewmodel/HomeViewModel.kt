package com.app.sneakers.featuresneaker.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.sneakers.core.network.Resource
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import com.app.sneakers.featuresneaker.domain.usecase.HomeUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val useCase: HomeUseCase) : ViewModel() {

    private val _isLoading = Channel<Boolean>()
    val isLoading = _isLoading.receiveAsFlow()

    private val _apiError = Channel<String>()
    val apiError = _apiError.receiveAsFlow()

    private var globalData = emptyList<SneakerListModel>()

    private val _filterListData = MutableStateFlow<List<SneakerListModel>?>(null)
    val filterListData = _filterListData.asStateFlow()

    private val _cartStatus = Channel<String?>()
    val cartStatus = _cartStatus.receiveAsFlow()

    init {
        getSneakerList()
    }

    private fun getSneakerList() {
        viewModelScope.launch(Dispatchers.IO) {
            useCase.getSneakersList().collectLatest {
                when (it) {
                    is Resource.Error -> {
                        it.error?.let { it1 -> _apiError.send(it1) }
                    }
                    is Resource.Loading -> {
                        _isLoading.send(it.isLoading)
                    }
                    is Resource.Success -> {
                        globalData = it.data
                        _filterListData.emit(it.data)
                    }
                }
            }
        }
    }

    fun insertCart(id : String) {
        viewModelScope.launch(Dispatchers.IO) {
            useCase.insertCart(id).collectLatest {
                when (it) {
                    is Resource.Error -> {
                        it.error?.let { it1 -> _apiError.send(it1) }
                    }
                    is Resource.Loading -> {
                        _isLoading.send(it.isLoading)
                    }
                    is Resource.Success -> {
                        _cartStatus.send(it.data)
                    }
                }
            }
        }
    }

    fun searchItem(query : String) {
        _filterListData.value = globalData.filter { it.title.contains(query,true) }
    }
}