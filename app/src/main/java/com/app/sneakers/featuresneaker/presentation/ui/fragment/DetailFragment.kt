package com.app.sneakers.featuresneaker.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import coil.load
import com.app.sneakers.R
import com.app.sneakers.core.base.BaseFragment
import com.app.sneakers.core.collectLatestLifecycleFlow
import com.app.sneakers.databinding.DetailFragmentBinding
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import com.app.sneakers.featuresneaker.presentation.viewmodel.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : BaseFragment() {

    private lateinit var binding : DetailFragmentBinding
    private val viewModel : DetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DetailFragmentBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        collectLatestLifecycleFlow(viewModel.isLoading, viewLifecycleOwner) {
            showProgress(it)
        }

        collectLatestLifecycleFlow(viewModel.apiError, viewLifecycleOwner) {
            showErrorMessage(it,binding.root)
        }
        collectLatestLifecycleFlow(viewModel.sneakerData, viewLifecycleOwner) {data->
            data?.let { setUi(it) }
        }
        collectLatestLifecycleFlow(viewModel.cartStatus, viewLifecycleOwner) {
            showSnackBar(it)
            Navigation.findNavController(binding.root).navigate(R.id.action_detailFragment_to_cartFragment)
        }

    }

    private fun setUi(sneakerListModel: SneakerListModel) {
        binding.tvTitle.text = sneakerListModel.title
        binding.tvBrand.text = "Brand : ${sneakerListModel.brand}"
        binding.tvDescription.text = sneakerListModel.name
        binding.tvYear.text = "Model : ${sneakerListModel.year}"
        binding.tvAmount.text = "Price : $${sneakerListModel.retailPrice}"
        binding.ivImage.load(sneakerListModel.media?.thumbUrl)
    }

    private fun showProgress(isShown: Boolean) {
        binding.progressBar.isVisible = isShown
    }

    override fun setOnclickListener() {
        binding.ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.btnCheckout.setOnClickListener {
            viewModel.addToCart()
        }
    }
}