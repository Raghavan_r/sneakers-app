package com.app.sneakers.featuresneaker.presentation.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.sneakers.core.network.Resource
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import com.app.sneakers.featuresneaker.domain.usecase.HomeUseCase
import com.app.sneakers.featuresneaker.presentation.utils.ARG_ID
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val useCase: HomeUseCase,
    stateHandle: SavedStateHandle
) : ViewModel() {

    private val productId = stateHandle.get<String>(ARG_ID)

    private val _isLoading = Channel<Boolean>()
    val isLoading = _isLoading.receiveAsFlow()

    private val _apiError = Channel<String>()
    val apiError = _apiError.receiveAsFlow()

    private val _sneakerData = MutableStateFlow<SneakerListModel?>(null)
    val sneakerData = _sneakerData.asStateFlow()

    private val _cartStatus = Channel<String>()
    val cartStatus = _cartStatus.receiveAsFlow()

    init {
        productId?.let { getSingleSneaker(it) }
    }

    private fun getSingleSneaker(id: String) {

        viewModelScope.launch(Dispatchers.IO) {
            useCase.getSingleSneaker(id).collectLatest {
                when (it) {
                    is Resource.Error -> {
                        it.error?.let { it1 -> _apiError.send(it1) }
                    }
                    is Resource.Loading -> {
                        _isLoading.send(it.isLoading)
                    }
                    is Resource.Success -> {
                        _sneakerData.emit(it.data)
                    }
                }
            }
        }

    }

    fun addToCart() {
        viewModelScope.launch {
            productId?.let { useCase.insertCart(it).collectLatest {
                when(it){
                    is Resource.Error -> {
                        it.error?.let { it1 -> _apiError.send(it1) }
                    }
                    is Resource.Loading -> {
                        _isLoading.send(it.isLoading)
                    }
                    is Resource.Success -> {
                        _cartStatus.send(it.data)
                    }
                }
            } }
        }

    }
}