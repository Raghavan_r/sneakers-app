package com.app.sneakers.featuresneaker.data.datasource

import android.util.Log
import com.app.sneakers.core.network.Resource
import com.app.sneakers.db.AppPreference
import com.app.sneakers.db.STORE_CART_DATA
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import com.app.sneakers.featuresneaker.presentation.utils.SOMETHING_WRONG
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import javax.inject.Inject


class HomeDataSource @Inject constructor(private val sharedPref : AppPreference){

    fun getSneakersListFromDataSource() : Resource<List<SneakerListModel>> {
        return try {
            Resource.Success(getSneakerList())
        }catch (e : Exception){
            Resource.Error("Something went wrong")
        }
    }

    private fun getSneakerList(): List<SneakerListModel> {
        val arrayItems: List<SneakerListModel>
        val gson = Gson()
        val type: Type = object : TypeToken<List<SneakerListModel>>() {}.type
        arrayItems = gson.fromJson<List<SneakerListModel>>(globalData, type)
        return arrayItems
    }

    fun getSingleSneakerFromDataSource(id : String) : Resource<SneakerListModel> {
        return try {
            val data = getSneakerList().find { it.id == id }
            data?.let {
                Resource.Success(it)
            } ?: Resource.Error("No Data Found")
        }catch (e : Exception){
            Resource.Error("Something went wrong")
        }
    }

    fun insertCartData(id : String) : Resource<String>{
        val cartList = sharedPref.getModelValue<List<String>>(STORE_CART_DATA)
        val data = cartList?.toMutableList() ?: emptyList<String>().toMutableList()
           return if (data.contains(id).not()){
                data.add(id)
                sharedPref.storeModelValue(data, STORE_CART_DATA)
                Resource.Success("Added to cart Successfully")
            }else{
                Resource.Error("Item already added to cart Successfully")
            }
    }

    fun getCartData(): Resource<List<SneakerListModel>>{
        val globalData = getSneakerList()
        val cartItems = sharedPref.getModelValue<List<String>>(STORE_CART_DATA)?.toMutableList() ?: emptyList()
        val outList : MutableList<SneakerListModel> = emptyList<SneakerListModel>().toMutableList()
        globalData.forEach { global ->
            cartItems.forEach { cart ->
                if (global.id == cart){
                    outList.add(global)
                }
            }
        }
        return if (outList.isEmpty().not()){
            Resource.Success(outList)
        }else{
            Resource.Error(SOMETHING_WRONG)
        }
    }

    fun removeItem(id : String) : Resource<String>{
        val cartItems = sharedPref.getModelValue<List<String>>(STORE_CART_DATA)?.toMutableList() ?: emptyList<String>().toMutableList()
        cartItems.remove(id)
        return try {
            sharedPref.storeModelValue(cartItems, STORE_CART_DATA)
            Resource.Success("Item Removed successfully")
        }catch (e :Exception){
            Resource.Error(SOMETHING_WRONG)
        }

    }

    fun clearCart() {
        sharedPref.storeModelValue(emptyList<String>(), STORE_CART_DATA)
    }

    private val globalData = "[\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\",\n" +
            "    \"brand\": \"Nike\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/mcqJV6zW/shoe-1.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/mcqJV6zW/shoe-1.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/mcqJV6zW/shoe-1.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2019-03-22\",\n" +
            "    \"retailPrice\": 1000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Pro version\",\n" +
            "    \"title\": \"Nike Air\",\n" +
            "    \"year\": 2019\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa7\",\n" +
            "    \"brand\": \"Crocs\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/VrCThBnr/shoe-2.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/VrCThBnr/shoe-2.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/VrCThBnr/shoe-2.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2020-02-15\",\n" +
            "    \"retailPrice\": 2000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Mid senior\",\n" +
            "    \"title\": \"Crocs Slider\",\n" +
            "    \"year\": 2020\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa8\",\n" +
            "    \"brand\": \"Crocs\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/Sj7rZtHF/27428-5-nike-shoes-transparent-background.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/Sj7rZtHF/27428-5-nike-shoes-transparent-background.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/Sj7rZtHF/27428-5-nike-shoes-transparent-background.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2020-02-15\",\n" +
            "    \"retailPrice\": 3000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Crocs push\",\n" +
            "    \"title\": \"Crocs Pro\",\n" +
            "    \"year\": 2020\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa9\",\n" +
            "    \"brand\": \"Addidas\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/68SL2s4R/28090-6-sneaker-file.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/68SL2s4R/28090-6-sneaker-file.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/68SL2s4R/28090-6-sneaker-file.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2022-12-15\",\n" +
            "    \"retailPrice\": 4000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Comfort Pro\",\n" +
            "    \"title\": \"Pro-res\",\n" +
            "    \"year\": 2022\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa10\",\n" +
            "    \"brand\": \"Addidas\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/F7Txvk9T/28116-8-nike-shoes.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/F7Txvk9T/28116-8-nike-shoes.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/F7Txvk9T/28116-8-nike-shoes.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2020-05-06\",\n" +
            "    \"retailPrice\": 5000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Max pro\",\n" +
            "    \"title\": \"Runner\",\n" +
            "    \"year\": 2020\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa11\",\n" +
            "    \"brand\": \"Addidas\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/pmyJLVrR/55532-3-sneakers-hd-image-free-png.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/pmyJLVrR/55532-3-sneakers-hd-image-free-png.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/pmyJLVrR/55532-3-sneakers-hd-image-free-png.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2020-08-11\",\n" +
            "    \"retailPrice\": 6000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"stylish fit\",\n" +
            "    \"title\": \"walf fit\",\n" +
            "    \"year\": 2020\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa12\",\n" +
            "    \"brand\": \"Addidas\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/Sj7rZtHF/27428-5-nike-shoes-transparent-background.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/Sj7rZtHF/27428-5-nike-shoes-transparent-background.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/Sj7rZtHF/27428-5-nike-shoes-transparent-background.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2021-04-22\",\n" +
            "    \"retailPrice\": 7000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Ready to walk\",\n" +
            "    \"title\": \"Hurricane\",\n" +
            "    \"year\": 2021\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa13\",\n" +
            "    \"brand\": \"Crocs\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/68SL2s4R/28090-6-sneaker-file.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/68SL2s4R/28090-6-sneaker-file.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/68SL2s4R/28090-6-sneaker-file.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2021-03-21\",\n" +
            "    \"retailPrice\": 8000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Wolfer pro\",\n" +
            "    \"title\": \"Crocs hanger\",\n" +
            "    \"year\": 2021\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa14\",\n" +
            "    \"brand\": \"Nike\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/F7Txvk9T/28116-8-nike-shoes.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/F7Txvk9T/28116-8-nike-shoes.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/F7Txvk9T/28116-8-nike-shoes.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2022-03-13\",\n" +
            "    \"retailPrice\": 9000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Nike Step\",\n" +
            "    \"title\": \"Step out\",\n" +
            "    \"year\": 2022\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa15\",\n" +
            "    \"brand\": \"Nike\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/pmyJLVrR/55532-3-sneakers-hd-image-free-png.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/pmyJLVrR/55532-3-sneakers-hd-image-free-png.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/pmyJLVrR/55532-3-sneakers-hd-image-free-png.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2022-02-25\",\n" +
            "    \"retailPrice\": 10000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Nike Fun\",\n" +
            "    \"title\": \"Step reach\",\n" +
            "    \"year\": 2022\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa16\",\n" +
            "    \"brand\": \"Nike\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/Sj7rZtHF/27428-5-nike-shoes-transparent-background.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/Sj7rZtHF/27428-5-nike-shoes-transparent-background.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/Sj7rZtHF/27428-5-nike-shoes-transparent-background.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2022-05-01\",\n" +
            "    \"retailPrice\": 11000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"Fitter type\",\n" +
            "    \"title\": \"Max pro nike\",\n" +
            "    \"year\": 2022\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa17\",\n" +
            "    \"brand\": \"Crocs\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/F7Txvk9T/28116-8-nike-shoes.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/F7Txvk9T/28116-8-nike-shoes.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/F7Txvk9T/28116-8-nike-shoes.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2021-09-15\",\n" +
            "    \"retailPrice\": 12000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"smooth walk\",\n" +
            "    \"title\": \"Crocs shoe\",\n" +
            "    \"year\": 2021\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa18\",\n" +
            "    \"brand\": \"Crocs\",\n" +
            "    \"colorway\": \"string\",\n" +
            "    \"gender\": \"Male\",\n" +
            "    \"media\": {\n" +
            "      \"imageUrl\": \"https://i.postimg.cc/pmyJLVrR/55532-3-sneakers-hd-image-free-png.png\",\n" +
            "      \"smallImageUrl\": \"https://i.postimg.cc/pmyJLVrR/55532-3-sneakers-hd-image-free-png.png\",\n" +
            "      \"thumbUrl\": \"https://i.postimg.cc/pmyJLVrR/55532-3-sneakers-hd-image-free-png.png\"\n" +
            "    },\n" +
            "    \"releaseDate\": \"2022-12-15\",\n" +
            "    \"retailPrice\": 13000,\n" +
            "    \"styleId\": \"string\",\n" +
            "    \"shoe\": \"string\",\n" +
            "    \"name\": \"shoe fit\",\n" +
            "    \"title\": \"Crocs run\",\n" +
            "    \"year\": 2022\n" +
            "  }\n" +
            "]"
}