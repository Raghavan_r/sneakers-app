package com.app.sneakers.featuresneaker.domain.repo

import com.app.sneakers.core.network.Resource
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import kotlinx.coroutines.flow.Flow

interface HomeRepo {
    suspend fun getSneakersList() : Flow<Resource<List<SneakerListModel>>>

    suspend fun getSingleSneaker(id : String) : Flow<Resource<SneakerListModel>>

    suspend fun insertCart(id : String) : Flow<Resource<String>>
}