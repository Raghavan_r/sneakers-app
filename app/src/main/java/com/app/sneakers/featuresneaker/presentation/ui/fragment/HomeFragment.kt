package com.app.sneakers.featuresneaker.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView.OnQueryTextListener
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.app.sneakers.R
import com.app.sneakers.core.base.BaseFragment
import com.app.sneakers.core.collectLatestLifecycleFlow
import com.app.sneakers.databinding.HomeFragmentBinding
import com.app.sneakers.featuresneaker.presentation.ui.adapter.HomeAdapter
import com.app.sneakers.featuresneaker.presentation.utils.ARG_ID
import com.app.sneakers.featuresneaker.presentation.utils.Type
import com.app.sneakers.featuresneaker.presentation.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class HomeFragment : BaseFragment() {

    private lateinit var binding: HomeFragmentBinding
    private val viewModel: HomeViewModel by viewModels()

    private val adapter = HomeAdapter { id, type ->
        id?.let { processClick(it, type) }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeFragmentBinding.inflate(
            inflater,
            container,
            false
        )
        binding.rvSneakers.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        collectLatestLifecycleFlow(viewModel.filterListData, viewLifecycleOwner) {
            it?.let {
                adapter.submitList(it)
            }
        }

        collectLatestLifecycleFlow(viewModel.cartStatus, viewLifecycleOwner) {
            it?.let {
                showSnackBar(it)
            }
        }

        collectLatestLifecycleFlow(viewModel.isLoading, viewLifecycleOwner) {
            showProgress(it)
        }

        collectLatestLifecycleFlow(viewModel.apiError, viewLifecycleOwner) {
            showErrorMessage(it,binding.root)
        }
    }


    private fun showProgress(isShown: Boolean) {
        binding.progressBar.isVisible = isShown
    }

    override fun setOnclickListener() {
        binding.ivCart.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_homeFragment_to_cartFragment)
        }

        binding.search.setOnQueryTextListener(object  : androidx.appcompat.widget.SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                p0?.let { viewModel.searchItem(it) }
                return false
            }

        })
    }

    private fun processClick(id: String, type: Type) {
        when (type) {
            Type.CART -> {
                viewModel.insertCart(id)
            }
            Type.DETAILS -> {
                Navigation.findNavController(binding.root).navigate(R.id.action_homeFragment_to_detailFragment,Bundle().apply {
                    this.putString(ARG_ID, id)})
            }
        }
    }
}