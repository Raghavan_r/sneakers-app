package com.app.sneakers.featuresneaker.data.repo

import android.util.Log
import com.app.sneakers.core.network.Resource
import com.app.sneakers.core.applyCommonSideEffects
import com.app.sneakers.featuresneaker.data.datasource.HomeDataSource
import com.app.sneakers.featuresneaker.domain.repo.HomeRepo
import com.app.sneakers.featuresneaker.presentation.utils.SOMETHING_WRONG
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class HomeRepoImpl @Inject constructor(private val dataSource: HomeDataSource) : HomeRepo {

    override suspend fun getSneakersList() = flow {
        emit(dataSource.getSneakersListFromDataSource())
    }.applyCommonSideEffects().catch { emit(Resource.Error(SOMETHING_WRONG)) }

    override suspend fun getSingleSneaker(id: String) = flow {
        emit(dataSource.getSingleSneakerFromDataSource(id))
    }.applyCommonSideEffects().catch { emit(Resource.Error(SOMETHING_WRONG)) }

    override suspend fun insertCart(id: String) = flow {
        try {
            emit(dataSource.insertCartData(id))
        }catch (e :Exception){
            emit(Resource.Error("Error in adding cart"))
        }
    }.applyCommonSideEffects().catch { emit(Resource.Error(SOMETHING_WRONG)) }

}