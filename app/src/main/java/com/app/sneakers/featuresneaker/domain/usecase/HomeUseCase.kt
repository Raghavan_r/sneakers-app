package com.app.sneakers.featuresneaker.domain.usecase

import com.app.sneakers.featuresneaker.domain.repo.HomeRepo
import javax.inject.Inject

class HomeUseCase @Inject constructor(private val homeRepo: HomeRepo) {

    suspend fun getSneakersList() = homeRepo.getSneakersList()

    suspend fun getSingleSneaker(id : String) = homeRepo.getSingleSneaker(id)

    suspend fun insertCart(id : String) = homeRepo.insertCart(id)
}