package com.app.sneakers.featuresneaker.presentation.utils

enum class Type {
    CART, DETAILS
}
enum class ActionType {
    CHECKOUT, CLEAR
}
const val SOMETHING_WRONG = "Something went wrong"
const val ARG_ID = "product id"