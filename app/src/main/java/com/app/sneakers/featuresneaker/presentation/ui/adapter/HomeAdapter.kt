package com.app.sneakers.featuresneaker.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.app.sneakers.databinding.ItemSneakersBinding
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import com.app.sneakers.featuresneaker.presentation.utils.Type

class HomeAdapter(
    private val listener : (id: String?,type : Type) -> Unit
) : ListAdapter<SneakerListModel, HomeAdapter.DataViewHolder>(REPO_COMPARATOR) {

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<SneakerListModel>() {
            override fun areItemsTheSame(
                oldItem: SneakerListModel,
                newItem: SneakerListModel
            ): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(
                oldItem: SneakerListModel,
                newItem: SneakerListModel
            ): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemSneakersBinding.inflate(layoutInflater, parent, false)
        return DataViewHolder(
            itemBinding
        )
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val item = getItem(position)
        if (item != null) {
            holder.bind(item)
        }
    }

    inner class DataViewHolder(
        private val itemBinding: ItemSneakersBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        @MainThread
        fun bind(data: SneakerListModel) {
            itemBinding.tvTitle.text = data.title
            itemBinding.tvPrice.text = "$${data.retailPrice.toString()}"
            itemBinding.ivProduct.load(data.media?.thumbUrl)
            itemBinding.clSneakerItem.setOnClickListener {
                listener.invoke(data.id,Type.DETAILS)
            }
            itemBinding.ivAdd.setOnClickListener{
                listener.invoke(data.id,Type.CART)
            }
        }
    }
}