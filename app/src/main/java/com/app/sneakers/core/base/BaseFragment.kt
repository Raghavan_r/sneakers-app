package com.app.sneakers.core.base

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.app.sneakers.R
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment : Fragment() {

    abstract fun setOnclickListener()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnclickListener()
    }

    fun showSnackBar(message: String) {
        Snackbar.make(
            requireView(),
            message,
            Snackbar.LENGTH_SHORT
        ).show()
    }

    protected fun showErrorMessage(message: String, view: View) {
        try {
            val snack = Snackbar.make(
                view,
                message,
                Snackbar.LENGTH_INDEFINITE
            )
            snack.setAction(resources.getString(R.string.ok)) {
                snack.dismiss()
            }
            snack.setActionTextColor(ContextCompat.getColor(requireContext(), R.color.orange_faded))
            snack.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}