package com.app.sneakers.core.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SneakerApp : Application()