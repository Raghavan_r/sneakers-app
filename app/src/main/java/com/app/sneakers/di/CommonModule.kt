package com.app.sneakers.di

import android.content.Context
import com.app.sneakers.db.AppPreference
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class CommonModule {
    @Provides
    @Singleton
    fun providePreferenceManager(@ApplicationContext context: Context): AppPreference =
        AppPreference(context)
}