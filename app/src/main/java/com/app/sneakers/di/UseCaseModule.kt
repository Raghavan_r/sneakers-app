package com.app.sneakers.di

import com.app.sneakers.featurecart.domain.repo.CartRepo
import com.app.sneakers.featurecart.domain.usecase.CartUseCase
import com.app.sneakers.featuresneaker.domain.repo.HomeRepo
import com.app.sneakers.featuresneaker.domain.usecase.HomeUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object UseCaseModule {

    @Provides
    @ViewModelScoped
    fun provideHomeUseCase(homeRepo : HomeRepo) = HomeUseCase(homeRepo)

    @Provides
    @ViewModelScoped
    fun provideCartUseCase(cartRepo: CartRepo) = CartUseCase(cartRepo)
}