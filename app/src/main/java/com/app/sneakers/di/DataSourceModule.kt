package com.app.sneakers.di

import com.app.sneakers.db.AppPreference
import com.app.sneakers.featuresneaker.data.datasource.HomeDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object DataSourceModule {

    @Provides
    fun provideHomeDataSource(preference: AppPreference): HomeDataSource {
        return HomeDataSource(preference)
    }

}