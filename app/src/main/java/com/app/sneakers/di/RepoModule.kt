package com.app.sneakers.di

import com.app.sneakers.featurecart.data.repo.CartRepoImpl
import com.app.sneakers.featurecart.domain.repo.CartRepo
import com.app.sneakers.featuresneaker.data.repo.HomeRepoImpl
import com.app.sneakers.featuresneaker.domain.repo.HomeRepo
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepoModule {

    @Singleton
    @Binds
    abstract fun provideHomeRepo(homeRepo: HomeRepoImpl): HomeRepo

    @Singleton
    @Binds
    abstract fun provideCartRepo(cartRepoImpl: CartRepoImpl): CartRepo

}