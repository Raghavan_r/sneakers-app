package com.app.sneakers.featurecart.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.app.sneakers.databinding.ItemCartBinding
import com.app.sneakers.featuresneaker.data.model.SneakerListModel

class CartItemAdapter(
    private val listener : (id: String) -> Unit,
) : ListAdapter<SneakerListModel, CartItemAdapter.DataViewHolder>(REPO_COMPARATOR) {

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<SneakerListModel>() {
            override fun areItemsTheSame(
                oldItem: SneakerListModel,
                newItem: SneakerListModel
            ): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(
                oldItem: SneakerListModel,
                newItem: SneakerListModel
            ): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemCartBinding.inflate(layoutInflater, parent, false)
        return DataViewHolder(
            itemBinding
        )
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val item = getItem(position)
        if (item != null) {
            holder.bind(item)
        }
    }

    inner class DataViewHolder(
        private val itemBinding: ItemCartBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        @MainThread
        fun bind(data: SneakerListModel) {
            itemBinding.ivProduct.load(data.media?.thumbUrl)
            itemBinding.tvTitle.text = data.title
            itemBinding.tvModel.text = data.name
            itemBinding.tvPrice.text = "$${data.retailPrice.toString()}"
            itemBinding.ivClose.setOnClickListener {
                data.id?.let { it1 -> listener.invoke(it1) }
            }
        }
    }
}