package com.app.sneakers.featurecart.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.sneakers.core.network.Resource
import com.app.sneakers.featurecart.domain.usecase.CartUseCase
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(private val useCase: CartUseCase) : ViewModel() {

    private val _isLoading = Channel<Boolean>()
    val isLoading = _isLoading.receiveAsFlow()

    private val _apiError = Channel<String>()
    val apiError = _apiError.receiveAsFlow()

    private val _cartData = MutableStateFlow<List<SneakerListModel>>(emptyList())
    val cartData = _cartData.asStateFlow()

    private val _removeData = Channel<String>()
    val removeData = _removeData.receiveAsFlow()

    init {
        getCartData()
    }

    private fun getCartData(){
        viewModelScope.launch {
            useCase.getCartData().collectLatest {
                    when(it){
                        is Resource.Error -> {
                            it.error?.let { it1 -> _apiError.send(it1) }
                        }
                        is Resource.Loading -> {
                            _isLoading.send(it.isLoading)
                        }
                        is Resource.Success -> {
                            _cartData.emit(it.data)
                        }
                    }
            }
        }
    }

    fun removeItem(id: String) {
        viewModelScope.launch {
            useCase.removeItem(id).collectLatest{
                when(it){
                    is Resource.Error -> {
                        it.error?.let { it1 -> _apiError.send(it1) }
                    }
                    is Resource.Loading -> {
                        _isLoading.send(it.isLoading)
                    }
                    is Resource.Success -> {
                        _removeData.send(it.data)
                        removeMainList(id)
                    }
                }
            }
        }
    }

    private fun removeMainList(id : String) {
        val data = _cartData.value.toMutableList()
        data.remove(data.find { it.id == id })
        _cartData.value = data
    }

    fun getSubTotal() =
        _cartData.value.sumOf { it.retailPrice ?: 0 }

    fun getTax() =
        (getSubTotal() *2) / 100

    fun getTotal() = getSubTotal() + getTax()

    fun clearCart() {
        useCase.clearCart()
        _cartData.value = emptyList()
    }

}