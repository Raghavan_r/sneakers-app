package com.app.sneakers.featurecart.data.repo

import com.app.sneakers.core.applyCommonSideEffects
import com.app.sneakers.core.network.Resource
import com.app.sneakers.featurecart.domain.repo.CartRepo
import com.app.sneakers.featuresneaker.data.datasource.HomeDataSource
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import com.app.sneakers.featuresneaker.presentation.utils.SOMETHING_WRONG
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class CartRepoImpl @Inject constructor(private val dataSource: HomeDataSource) : CartRepo {

    override suspend fun getCartData() = flow {
        emit(dataSource.getCartData())
    }.applyCommonSideEffects().catch { emit(Resource.Error(SOMETHING_WRONG)) }

    override suspend fun removeItem(id : String) = flow {
        emit(dataSource.removeItem(id))
    }.applyCommonSideEffects().catch { emit(Resource.Error(SOMETHING_WRONG)) }

    override fun clearCart() {
        dataSource.clearCart()
    }
}