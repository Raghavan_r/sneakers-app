package com.app.sneakers.featurecart.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.app.sneakers.core.base.BaseFragment
import com.app.sneakers.core.collectLatestLifecycleFlow
import com.app.sneakers.databinding.CartFragmentBinding
import com.app.sneakers.featurecart.presentation.ui.adapter.CartItemAdapter
import com.app.sneakers.featurecart.presentation.viewmodel.CartViewModel
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CartFragment : BaseFragment() {

    private lateinit var binding : CartFragmentBinding
    private val viewModel : CartViewModel by viewModels()

    private val adapter = CartItemAdapter{id->
        viewModel.removeItem(id)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CartFragmentBinding.inflate(
            inflater,
            container,
            false
        )
        binding.rvCart.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        collectLatestLifecycleFlow(viewModel.cartData, viewLifecycleOwner) {
            setUI(it)
        }

        collectLatestLifecycleFlow(viewModel.removeData, viewLifecycleOwner) {
            showSnackBar(it)
        }

        collectLatestLifecycleFlow(viewModel.isLoading, viewLifecycleOwner) {
            showProgress(it)
        }

        collectLatestLifecycleFlow(viewModel.apiError, viewLifecycleOwner) {
            showErrorMessage(it,binding.root)
        }

    }

    private fun setUI(data: List<SneakerListModel>) {
        if (data.isEmpty()){
            binding.clCart.isGone = true
            binding.tvNoItems.isVisible = true
        }else{
            binding.clCart.isVisible = true
            binding.tvNoItems.isGone = true
            adapter.submitList(data)
            binding.tvSubTotal.text = "Subtotal: "+viewModel.getSubTotal()
            binding.tvTax.text = "Taxes and Charges : $"+viewModel.getTax()
            binding.tvTotalValue.text = "$"+viewModel.getTotal()
            binding.btnCheckout.text = "$ "+viewModel.getTotal()+" Checkout"
        }
    }

    override fun setOnclickListener() {
        binding.btnCheckout.setOnClickListener {
            showSnackBar("Order Placed Successfully")
            viewModel.clearCart()
        }
        binding.ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun showProgress(isShown: Boolean) {
        binding.progressBar.isVisible = isShown
    }

}