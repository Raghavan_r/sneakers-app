package com.app.sneakers.featurecart.domain.repo

import com.app.sneakers.core.network.Resource
import com.app.sneakers.featuresneaker.data.model.SneakerListModel
import kotlinx.coroutines.flow.Flow

interface CartRepo {
    suspend fun getCartData() : Flow<Resource<List<SneakerListModel>>>
    suspend fun removeItem(id : String) : Flow<Resource<String>>
    fun clearCart()
}