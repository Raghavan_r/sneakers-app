package com.app.sneakers.featurecart.domain.usecase

import com.app.sneakers.featurecart.domain.repo.CartRepo
import javax.inject.Inject

class CartUseCase @Inject constructor(private val cartRepo: CartRepo) {

    suspend fun getCartData() = cartRepo.getCartData()

    suspend fun removeItem(id : String) = cartRepo.removeItem(id)

    fun clearCart() {
        cartRepo.clearCart()
    }

}